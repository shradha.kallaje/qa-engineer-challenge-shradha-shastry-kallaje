"""This module includes tests for download flows

Test paths:
    - TestNoConnection: A download request with no internet connection results in getting data from the LocalCache.
    - TestGetProtocol: Happy path for downloading protocols from the CustomerDB
"""

import pytest
from utility_library.broker import MessageBroker
from utility_library.local_cache import LocalCache
import utility_library.sample_data as sample_data
import utility_library.networking as networking

sample_rgb_image = {
    "id": "123456",
    "timestamp": "2020-06-02T07:56:02.832000",
    "dataPath": "https://test-data.dev/123456",
    "eggs": [
        "1",
        "2",
        "3",
        "4",
        "5",
    ],
}


class TestGetImage:
    'Happy path for downloading images from the DB'

    def test_get_protocol(self):
        """Happy path for downloading images from the DB

        """

        # Initialize DB with dummy contents
        sample_data.initialize_database_with_content(sample_rgb_image)

        # Expected mage to be gotten
        expected_result = sample_rgb_image.copy()

        # Create download request
        broker = MessageBroker()
        event = "DOWNLOAD_RGB_IMAGE"
        body = {
            "id": sample_rgb_image["id"],
        }
        corr_id = broker.publish_event(event, body)

        # Wait for response from the data downloader
        response_event, response_body = broker.wait_for_response(corr_id)

        # Event as expected
        assert response_event == "DOWNLOAD_RGB_IMAGE_RESPONSE"

        # Data should be from the cloud
        assert response_body[1] == True

        # Data should match the expected data
        assert expected_result == response_body[0]

        # Empty DB
        sample_data.empty_database_content()


class TestNoConnection:
    'A download request with no internet connection results in getting data from the LocalCache'

    def test_no_connection(self):
        """A download request with no internet connection results in getting data from the LocalCache

        A download request for an image is made. After that, the gotten data is compared to the data that should be
        gotten from Local Cache.
        """

        # Insert a batch in the Local Cache
        LocalCache.images.delete_many({"id": sample_rgb_image["id"]})
        LocalCache.insert_image(sample_rgb_image.copy())

        # Create Network Policy that blocks external traffic for the data-downloader
        networking.block_traffic()

        # Create download request
        broker = MessageBroker()
        event = "DOWNLOAD_RGB_IMAGE"
        body = {
            "id": sample_rgb_image["id"],
        }
        corr_id = broker.publish_event(event, body)

        # Wait for response from the data downloader
        response_event, response_body = broker.wait_for_response(corr_id)

        assert response_event == "DOWNLOAD_RGB_IMAGE_RESPONSE"
        assert response_body[1] == False
        assert response_body[0] == sample_rgb_image

        # Recover connection
        networking.unblock_traffic()

        # Delete batch from Local Cache
        LocalCache.images.delete_one({"id": sample_rgb_image["id"]})
