"""Implementation of the download that gets images
"""

import requests
from utility_library.local_cache import LocalCache


class GetImage():
    """Download RGB image
    """
    def download(self, id):
        """Use perform a GET request to get an image

        Args:
            id (str): ID of the image to download

        Returns:
            tuple:
                - Event name of the response
                - tuple:
                    - image data
                    - whether or not the image comes from the cloud
        """

        print("Downloading Image")

        try:
            response = requests.get(f"https://api.dev/rgb-images/{id}")
            image = response.json()
            from_cloud = True
            LocalCache.update_images(id, image)

        # If failed, get it from the Local Cache or raise error
        except requests.exceptions.ConnectionError:

            print("Failed request. Data attempted to get from Local Cache")
            image = LocalCache.get_images(id)
            from_cloud = False

        return "DOWNLOAD_RGB_IMAGE_RESPONSE", (image, from_cloud)
