import os
import requests


def get_first_scan(api_host, api_headers_accept):
    """Gets the first element of the scans stored in the DB

    Args:
        api_host (str): API host (e.g. https://orbem-api.dev)
        api_headers_accept (str): Accept field for the get request
    """

    # Get a list of current scans in the DB
    r = requests.get(f"{api_host}/scans",
                     headers={"Accept": api_headers_accept},
                     timeout=1)

    # First scan
    scans = r.json()

    return scans[0]


if __name__ == "__main__":

    # Get host and headers from environment variables
    api_host = os.getenv("API_HOST")
    api_headers_accept = os.getenv("API_HEADERS_ACCEPT")

    scan = get_first_scan(api_host, api_headers_accept)
    print("First scan:", scan['id'])