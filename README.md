# QA Engineer Challenge

Welcome to your challenge. This repository features several tasks you can work on during the next 90 minutes. Each task has a description, a deliverable and additional resources if necessary. We use story points to estimate the workload of a task. In this context, one story point roughly represents 10-15 minutes of work. This means you will likely not be able to complete all of the tasks.

We will use the last 15 minutes of the call to discuss your results. Please think about how you want to structure the presentation of your results. You will also have the opportunity to ask questions then.

## Task 1: Create automated test suite given product specifications

**Priority:** High

**Story points:** 5

**Description:** Given two features (user-stories) for the following product, define and pseudo-code a set of automatic tests.

The product is an egg tracking software. The software receives an RGB image of a tray of eggs similar to the one shown here:

<!-- ![image info](.resources/tray.jpg) -->
<img src=".resources/tray.jpg" alt="drawing" width="400" style="display: block; margin: auto;"/>

Each egg has a printed number that identifies it. Assume every printed number is different. The following user-stories must be tested:

* I want to have a single image for each egg
* I want to be able to know which egg is in which position of the tray

### Deliverable

* Document describing the automated test suite added to the repository (as a `pdf` in the `task-1` directory)
* List of automated tests written in pseudo code.

## Task 2: Report a bug given an error trace

**Priority:** Medium

**Story points:** 1

**Description:** Given the error trace, report the bug in the most descriptive way.

A REST API is used to get the first element of a DB that hosts Egg Scans. Running the code, however, results in an error. The code can be found in the `task-2` directory of this repository.

The traceback obtained during the execution of the code is:
<img src=".resources/traceback.png" alt="drawing" width="800" style="display: block; margin: auto;"/>

### Deliverable

* Bug reported in Gitlab Issues (Incident)

## Task 3: Support team member to modify a Gitlab CI pipeline

**Priority:** Low

**Story points:** 2

**Description:** A colleague from the software team needs to modify our current CI to fail the unit test step if coverage is lower than 80%. She has little experience with GitLab CI and comes to you seeking help and shows you the current CI, please advise her on how to proceed. The Merge Request also involves python code.

The Merge Request can be found in this same repository, with the name `Task 3`. Please make comments on the MR with your proposed solution.

### Deliverable

* Level of support to be assessed by the interviewer.
* Comments on the MR that reflect interaction and solutions

## Task 4: Prepare E2E testing session

**Priority:** High

**Story points:** 3

**Description:** Given a software and hardware release candidate, plan the necessary steps to run end to end tests, including the strategy for monitoring of the results.

The setup consists of an MRI-system that scans trays of eggs, one after the other. A touch-screen with a UI is used to interact with the system: starting the scan process and stopping the scan process. Whenever an error in the system occurs, the error traceback is shown in the UI, and the scanning automatically stops. After that, the scanning can be resumed manually through the UI.

A new feature was added recently at the SW and HW level: The scanning speed was incremented by 2x, by changing the type of MRI-hardware used. The SW that controls the new HW is still in development by us. We must see what kind of issues arise when the new HW is being used for a long time.

KPIs to monitor:
* Uptime
* Downtime
* \# of completely processed eggs
* \# of lost eggs
* \# of errors shown in UI
* Average time per tray
* Temperature of the system

### Deliverable

* Proposal document uploaded to Gitlab as a `pdf` in the `task-4` directory.

## Task 5: Maintenance of integration test suite

**Priority:** Medium

**Story points:** 3

**Description:** A new software release breaks the integration tests. Identify the changes and apply the necessary fixes to make the integration tests applicable.

The base software consists of a “downloader” service, which queries a REST API to get metadata of RGB images. Whenever there is no connection to the internet, the “downloader” attempts to get the data from a Local Cache DB.

The code of the downloader can be found in the `task-5/downloader` directory. Two integration tests are written for the downloader in the `task-5/tests` directory.

A Merge Request with the name `Task 5` is open with the changes to the downloader that broke the integration tests. Please update that Merge Request by applying the necessary fixes to the integration tests.

### Deliverable:

* MR with code changes