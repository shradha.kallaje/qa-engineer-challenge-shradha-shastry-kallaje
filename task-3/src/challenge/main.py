import time
from threading import Thread
from .utils import square_area, circle_area

queue = []


def produce(figure_area, parameters):
    """Given a list of geometrical figures and a list of input parameters, it 
    evaluates each entry in `figure_area` with the parameter in `parameters`, 
    and produces dictionaries in the form:
    
    `{'figure':<name of the called function> ,
    'parameter':<value of the parameter used> , 
    'area':<area of the given figure for the given parameter>}`

    Each dictionary is appended to queue and will be eventually consumed.

    Args:
        figure_area (list): list of callable functions to compute the area of a specific geomtric 
            figure: `.utils.circle_area` or `.utils.square_area`.
        parameters (list): list parameters for the functions in the figure_area list. There is a 
            one to one match based on the position.
    
    Raises:
        ValueError: if any of the parameters is `None` or empty. Or if the list `figure_area` 
            contain non-callable items, or the `parameters` list contains non-float elements.
    """

    if not figure_area:
        raise ValueError('Argument cannot be None or empty.')

    if not parameters:
        raise ValueError('Argument cannot be None or empty.')

    for p in range(len(parameters)):
        element = {
            'figure': figure_area[p].__name__,
            'parameter': parameters[p],
            'area': figure_area[p](parameters[p])
        }
        queue.append(element)
        print(f'Element: {element} added to the queue')
        time.sleep(0.5)


def consume():
    """Given a queue of dictionaries as described in `produce`. This method
    extract each element an print its content in the following format:

    'The {figure} with p={parameter} cm is {area} cm²'

    This methods consumes the elements in the queue until it is empty.
    """
    # TODO


if __name__ == '__main__':

    figures = [
        square_area, circle_area, circle_area, square_area, square_area,
        circle_area
    ]
    parameters = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0]

    Thread(target=produce, args=[figures, parameters]).start()